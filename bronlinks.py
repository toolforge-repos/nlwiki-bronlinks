import sys
import pywikibot
import json
import random

random.seed(0)

with open('bronlinks-2.json') as f:
    results = json.load(f)

processed = set(line.strip() for line in open('processed.txt'))
processed_fp = open('processed.txt', 'a')

random.shuffle(results)

print("Processing %d results" % len(results))

#start = int(sys.argv[1])
#maxnum = int(sys.argv[2])

#start = int(open('start').read())
#maxnum = start + 100

TEMPLATE = """== Referenties gevonden in artikelgeschiedenis ==
<!-- START: REFERENCES FOUND -->
Hallo medebewerkers,

Om de kwaliteit van bronvermeldingen binnen Wikipedia te verbeteren hebben we gekeken of er in de artikelgeschiedenis van dit artikel links naar externe webpagina's of naar andere wikis staan. In het verleden werd veel gebruik gemaakt van deze optie om de bron van een bewerking aan te geven, maar tegenwoordig worden bronnen meestal in het artikel zelf getoond. Het zou dus kunnen dat in de geschiedenis waardevolle bronnen staan die in het artikel zelf kunnen worden meegenomen. 

Meer informatie over dit project is terug te vinden in de [[Gebruiker:Valhallasw/bronlinks/faq|FAQ]].

In de artikelgeschiedenis van [[%s]] zijn de volgende bewerkingen gevonden:

%s

Zouden jullie kunnen kijken of deze links geschikt zijn om in de bronvermelding in het artikel mee te nemen? Bij voorbaat dank. 

Groet, ~~~~

''Als de bovenstaande bronnen zijn bekeken dan kan deze melding worden verwijderd. Als een lege overlegpagina overblijft dan kan deze met <nowiki>{{nuweg|afgehandelde botmelding}}</nowiki> voor verwijdering worden aangedragen.''
[[Categorie:Wikipedia:Zichtbaarheid van bronvermeldingen]]
<!-- END: REFERENCES FOUND -->
"""

s = pywikibot.Site('nl', 'wikipedia')

ctr = 0

for i, (entry, text) in enumerate(results):
#  if i <= start:
#    pywikibot.output(entry + ": SKIP due to start")
#    continue
#  if i >= maxnum:
#    break
  if entry in processed:
    pywikibot.output(entry + ": SKIP due to already processed")
    continue

  processed_fp.write(entry + "\n")
  processed_fp.flush()
  ctr += 1

  e = ("% 5d - %s" % (i, entry))

  p = pywikibot.Page(s, entry)
  tp = p.toggleTalkPage()

  text = "\n".join(text)

  oldtext = (tp.get() + "\n\n") if tp.exists() else ""
  if "START: REFERENCES FOUND" in oldtext or "Groet, valhallasw-toolserver-botje" in oldtext:
    pywikibot.output(e + ": SKIP due to existing text")
    continue

  if not tp.botMayEdit():
    pywikibot.output(e + ": SKIP due to nobots")

  pywikibot.output(e + ": PUT")
  tp.put(oldtext + TEMPLATE % (entry, text), summary="Bot: Referenties gevonden in artikelgeschiedenis", watch=False, minor=False)

  if ctr >= 50:
    break

